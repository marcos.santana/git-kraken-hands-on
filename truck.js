const net = require('net')
const ID_CAMINHAO = 2;
var isConnection = false;

const serverConnection = [
    {ip: '0.tcp.ngrok.io', port: 18246},
    {ip: '3.22.15.135', port: 15157},
    // {ip: '2.tcp.ngrok.io', port: 16643},
    // {ip: 'localhost', port: 80},
    // {ip: 'localhost', port: 81},
]

const nextConnection = (numberConnection) => {
    var client = net.connect(serverConnection[numberConnection].port, serverConnection[numberConnection].ip, () => {
        setTimeout(() => {
            if(!isConnection) {
                console.log(`NÃO CONECTOU COM ${serverConnection[numberConnection].ip}:${serverConnection[numberConnection].port}`)
                nextConnection(numberConnection + 1);
            }
        }, 5000);
    })

    client.on('data', data => {
        console.log(data.toString());
        if(data.toString().indexOf('CONECTOU') >= 0) {
            isConnection = true;
            client.write(`LIVRE ${ID_CAMINHAO}`);
        }
        if(data.toString().indexOf('COLETAR') >= 0) {
            setTimeout(() => {
                let m = `CHEGUEI_CONTAINER ${ID_CAMINHAO}`;
                console.log(m);
                client.write(m);
                setTimeout(() => {
                    let m1 = `COLETA_FINALIZADA ${ID_CAMINHAO}`;
                    console.log(m1);
                    client.write(m1);
                }, 1000 * Math.random() * (15) + 5);// 5 - 20 seconds
            }, 1000 * Math.random() * (15) + 5);// 5 - 20 seconds
            return;
        }
    })
    
    client.on('close', (data) => {
        client.write('.');
        console.log('.');
    }) 

    client.on('error', () => {
        console.log('Server error!');
        console.log('Next Server');
        nextConnection(numberConnection + 1);
    });
}

nextConnection(0)
