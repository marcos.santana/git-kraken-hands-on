const net = require('net');

const client = new net.Socket();

const PORT = 17515;
client.connect(PORT, "2.tcp.ngrok.io", () => {
	console.log('Connected');
	client.write('Olá Servidor');
});

client.on('data', (data) => {
	console.log('Recebido: ' + data);
});

client.on('close', () => {
	console.log('Conexão encerrada');
});

client.on('error', (error) => {
	console.log(error);
});
